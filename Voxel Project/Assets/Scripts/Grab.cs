﻿using UnityEngine;
using System.Collections;

public class Grab : MonoBehaviour {

    [SerializeField]
    SteamVR_TrackedController controller;
    [SerializeField]
    SteamVR_TrackedController controller2;
    [SerializeField]
    Fire fireCannon;
	// Use this for initialization
	void Start () {
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
        fireCannon.FireCannon();
        }
    }
	

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Controller")
        {
            if (controller.triggerPressed == true)
            {
               transform.position  = col.transform.position;
               if (controller.padPressed == true)
               {
                   fireCannon.FireCannon();
               }
            }
        }
    }
}
