﻿using UnityEngine;
using System.Collections;

public class CarReset : MonoBehaviour {

    public bool CheckPoint1 = false;
    public bool CheckPoint2 = false;
    [SerializeField]
    private Transform Location1;
    [SerializeField]
    private Transform Location2;
    [SerializeField]
    private GameObject Player;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.R))
        {
            reset();
        }
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Car")
        {
            reset();
        }
    }

    void reset()
    {
        Player.GetComponent<Rigidbody>().isKinematic = true;
        Player.transform.eulerAngles = new Vector3(0, 0, 0);
        if (CheckPoint2 == true)
        {      
            Player.transform.position = Location2.position;
        }
        else if (CheckPoint1 == true)
        {
            Player.transform.position = Location1.position;
        }
        else
        {
            Application.LoadLevel(1);
        }
        Player.GetComponent<Rigidbody>().isKinematic = false;
    }
}
