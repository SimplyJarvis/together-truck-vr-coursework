﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {
    [SerializeField]
    float speed = 10f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += new Vector3(0, 0, Input.GetAxis("cKeyboard") * speed);
	}
}
