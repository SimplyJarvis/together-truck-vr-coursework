﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

    [SerializeField]
    GameObject bulletPrefab;
    private AudioSource shootSound;

    private SteamVR_TrackedController device;
	// Use this for initialization
	void Start () {
        device = GetComponent<SteamVR_TrackedController>();
        device.TriggerClicked += Shooting;
        shootSound = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Shooting (object sender, ClickedEventArgs e) {
        Instantiate(bulletPrefab, transform.position, transform.rotation * Quaternion.Euler(60,0,0));
        shootSound.Play();
	}
}
