﻿using UnityEngine;
using System.Collections;

public class Rotating : MonoBehaviour {

    [SerializeField]
    Vector3 rotate;
    [SerializeField]
    float speed;
    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        rb.AddTorque(rotate * speed * Time.deltaTime);
        transform.Rotate(rotate * speed * Time.deltaTime);
	}
}
