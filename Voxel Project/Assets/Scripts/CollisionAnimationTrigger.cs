﻿using UnityEngine;
using System.Collections;

public class CollisionAnimationTrigger : MonoBehaviour {
    [SerializeField]
    GameObject ObjectToTrigger;
    void OnTriggerEnter(Collider col)
    {
        ObjectToTrigger.SendMessage("Trigger");
    }
}
