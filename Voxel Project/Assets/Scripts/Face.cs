﻿using UnityEngine;
using System.Collections;

public class Face : MonoBehaviour {
    [SerializeField]
    Transform target;
    [SerializeField]
    Quaternion refine;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(target.position);
        transform.rotation *= refine;
	}

}
