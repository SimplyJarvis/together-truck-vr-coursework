﻿using UnityEngine;
using System.Collections;

public class TargetTriggerRigidBodyEnable : MonoBehaviour {

    [SerializeField]
    Rigidbody fall;
    [SerializeField]
    GameObject object1;
    [SerializeField]
    GameObject object2;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            fall.isKinematic = false;
            Destroy(object1);
            Destroy(object2);
        }
    }


}
