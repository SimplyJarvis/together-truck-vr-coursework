﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TutorialController : MonoBehaviour {
    [SerializeField]
    Animator GuyAni;
    [SerializeField]
    AudioSource VoiceOver1;
    [SerializeField]
    AudioClip VoiceOver2;
    [SerializeField]
    GameObject box;
    [SerializeField]
    GameObject suggestion;
    [SerializeField]
    FaceSmooth Eye1;
    [SerializeField]
    FaceSmooth Eye2;
    [SerializeField]
    Transform target1;
    [SerializeField]
    Transform target2;
	// Use this for initialization
	void Start () {
        Invoke("Suggestion", 30f);
	}

    void ButtonHit()
    {
        GuyAni.SetInteger("Start", 1);
        VoiceOver1.Play();
        Invoke("Suggestion", 30f);
    }

    void Suggestion()
    {
        box.SetActive(true);
        suggestion.SetActive(true);
    }


    void Part2()
    {
        GuyAni.SetInteger("Start", 2);
        VoiceOver1.clip = VoiceOver2;
        VoiceOver1.Play();
        Invoke("EyeTarget", 40f);
        Invoke("nextLevel", 115f);
    }

    void EyeTarget()
    {
        Eye1.target = target2;
        Eye2.target = target2;
        Invoke("EyeReset", 40f);
    }

    void EyeReset()
    {
        Eye1.target = target1;
        Eye2.target = target1;
    }

    void nextLevel()
    {
        SceneManager.LoadScene(1);
    }
}
