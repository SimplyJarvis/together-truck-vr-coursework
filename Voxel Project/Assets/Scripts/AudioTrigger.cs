﻿using UnityEngine;
using System.Collections;

public class AudioTrigger : MonoBehaviour {

    [SerializeField]
    AudioSource sound;
    [SerializeField]
    ParticleSystem particle;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            sound.Play();
            particle.Play();
        }
    }
}
