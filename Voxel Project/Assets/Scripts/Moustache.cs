﻿using UnityEngine;
using System.Collections;

public class Moustache : MonoBehaviour {

    [SerializeField]
    AudioSource narate;
    bool called = false;
    [SerializeField]
    float smooth;
    float move;
	// Update is called once per frame
	void Update () {
        if (narate.isPlaying)
        {
            startInvoke();
        }
        else 
        {
            called = false;
        }
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, Mathf.Lerp(transform.eulerAngles.z, move, Time.deltaTime * smooth));
	}

    void MoustacheMove()
    {
        move = Random.Range(170f, 190f);
    }

    void startInvoke()
    {
        if (!called)
        {
            InvokeRepeating("MoustacheMove", 0, 0.2f);
            called = true;
        }
    }

}
