﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {
    [SerializeField]
    private int checkNum;
    [SerializeField]
    private CarReset checkpointReset;
    [SerializeField]
    GameObject uiInfo;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (checkNum == 1)
        {
            checkpointReset.CheckPoint1 = true;
            uiInfo.SetActive(true);
            Invoke("removeUI", 3f);
        }
        if (checkNum == 2)
        {
            checkpointReset.CheckPoint2 = true;
            uiInfo.SetActive(true);
            Invoke("removeUI", 3f);
        }
    }

    void removeUI()
    {
        uiInfo.SetActive(false);
    }
}
