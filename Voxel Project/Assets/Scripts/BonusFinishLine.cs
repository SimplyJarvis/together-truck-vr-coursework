﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BonusFinishLine : MonoBehaviour {
    [SerializeField]
    GameObject wall;
    [SerializeField]
    private AudioSource music;
    [SerializeField]
    private AudioSource musicToStop;
    [SerializeField]
    private ParticleSystem dolla;
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Car")
        {
            Destroy(wall);
            dolla.Play();
            music.Play();
            musicToStop.Stop();
            Invoke("BackToMenu", 20);
        }
    }

    void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
