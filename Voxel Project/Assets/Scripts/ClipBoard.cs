﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ClipBoard : MonoBehaviour {

    private AudioSource music;
    [SerializeField]
    private ParticleSystem dolla;
    private bool finish = false;
    [SerializeField]
    private GameObject text;
	// Use this for initialization
	void Start () {
        music = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if (finish == true)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                SceneManager.LoadScene(2);  
            }
        }
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Car")
        {
            music.Play();
            dolla.Play();
            text.SetActive(true);
            finish = true;
        }
    }


}
