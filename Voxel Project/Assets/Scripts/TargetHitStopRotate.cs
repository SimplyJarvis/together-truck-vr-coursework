﻿using UnityEngine;
using System.Collections;

public class TargetHitStopRotate : MonoBehaviour {
    [SerializeField]
    Rotating spinningBlock;
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            spinningBlock.enabled = false;
        }
    }
}
