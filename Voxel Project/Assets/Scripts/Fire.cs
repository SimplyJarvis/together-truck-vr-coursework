﻿using UnityEngine;
using System.Collections;

public class Fire : MonoBehaviour {
    [SerializeField]
    GameObject prefab;
    [SerializeField]
    Vector3 FireDirection;
    [SerializeField]
    float force;
    float timer = 5;
    [SerializeField]
    ParticleSystem shoot;
    [SerializeField]
    ParticleSystem shoot2;
    [SerializeField]
    AudioSource sound;

    private void Update()
    {
        timer += Time.deltaTime;
    }


    public void FireCannon()
    {
        if (timer > 3)
        {
            GameObject tempObj = Instantiate(prefab, transform.position, transform.rotation) as GameObject;
            var tempRB = tempObj.GetComponent<Rigidbody>();
            tempRB.AddForce(transform.forward * force);
            Destroy(tempObj, 5f);
            timer = 0;
            shoot.Play();
            shoot2.Play();
            sound.Play();
        }
    }
}
