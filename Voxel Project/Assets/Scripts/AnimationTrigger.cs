﻿using UnityEngine;
using System.Collections;

public class AnimationTrigger : MonoBehaviour {
    Animator ani;

    void Start()
    {
        ani = gameObject.GetComponent<Animator>();
    }

    void Trigger()
    {
        ani.SetBool("TargetHit", true);
    }
}
