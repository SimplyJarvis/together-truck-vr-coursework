﻿using UnityEngine;
using System.Collections;

public class PickUpAnything : MonoBehaviour {


    
    SteamVR_TrackedController controller;

    void Start()
    {
        controller = gameObject.GetComponent<SteamVR_TrackedController>();
    }
 




    void OnTriggerStay(Collider col)
    {
        if (col.tag == "Grabable" || col.tag == "Stamp" || col.tag == "Planks" || col.tag == "Suggest")
        {
            if (controller.triggerPressed == true)
            {
                if (col.transform.parent == null)
                {
                    col.transform.parent = transform;
                    col.isTrigger = true;
                    col.GetComponent<Rigidbody>().isKinematic = true;
                }
            }
            if (controller.triggerPressed == false)
            {
                col.transform.parent = null;
                col.isTrigger = false;
                if (col.tag != "Planks")
                {
                    col.GetComponent<Rigidbody>().isKinematic = false;
                }
                
            }
        }
    }
}
