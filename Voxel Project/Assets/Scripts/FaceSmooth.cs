﻿using UnityEngine;
using System.Collections;

public class FaceSmooth : MonoBehaviour {

    [SerializeField]
    public Transform target;
    [SerializeField]
    float smooth;
    [SerializeField]
    float refineY;
    // Update is called once per frame
    void Update()
    {
        Quaternion targetRotation = Quaternion.LookRotation(target.transform.position - transform.position);
        targetRotation *= Quaternion.Euler(0, refineY, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, smooth * Time.deltaTime);
    }
}
