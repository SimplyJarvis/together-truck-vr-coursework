﻿using UnityEngine;
using System.Collections;

public class VerticalCameraSmooth : MonoBehaviour {

    [SerializeField]
    Transform target;
    [SerializeField]
    float speed;
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 v = transform.position;
        v.y = Mathf.Lerp(v.y, target.position.y, Time.deltaTime * speed);
        v.x = target.position.x;
        v.y = target.position.z;
        transform.position = v;
	}
}
