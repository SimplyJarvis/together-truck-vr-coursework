﻿using UnityEngine;
using System.Collections;

public class CameraSmooth : MonoBehaviour {

    [SerializeField]
    Transform target;
    [SerializeField]
    float smoothTime = 0.3f;
    Vector3 velocity = Vector3.zero;
	
	// Update is called once per frame
	void FixedUpdate () {
	    Vector3 temp = Vector3.SmoothDamp(transform.position, target.position, ref velocity, smoothTime);
        transform.position = new Vector3(target.position.x, temp.y, target.position.z);
        transform.rotation = target.rotation;
	}
}
