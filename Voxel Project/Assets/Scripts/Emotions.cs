﻿using UnityEngine;
using System.Collections;

public class Emotions : MonoBehaviour {

    [SerializeField]
    [Range(0, 3)]
    int emotion = 0;
    
    [SerializeField]
    private Animator ani;

	// Use this for initialization
	void Start () {
        InvokeRepeating("RandomEmote", 0f, 4f);
	}
	
	// Update is called once per frame
	void Update () {
        setEmotion();
	}

    void setEmotion()
    {
        ani.SetInteger("Feels", emotion);
    }

    void RandomEmote()
    {
        emotion = Random.Range(0, 3);
    }
}
