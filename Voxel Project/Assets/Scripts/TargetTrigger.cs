﻿using UnityEngine;
using System.Collections;

public class TargetTrigger : MonoBehaviour {

    [SerializeField]
    GameObject target;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            target.SendMessage("Trigger");
        }
    }
}
