﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChaseWall : MonoBehaviour {
    [SerializeField]
    Vector3 Direction;
    [SerializeField]
    float speed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += (Direction * speed) * Time.deltaTime;
	}

    void OnCollisionEnter(Collision col)
    {
        SceneManager.LoadScene(0);
        Destroy(gameObject);
    }
}
