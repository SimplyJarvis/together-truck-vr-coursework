﻿using UnityEngine;
using System.Collections;

public class SuggestionBox : MonoBehaviour {
    [SerializeField]
    TutorialController tut;
    [SerializeField]
    GameObject sug;
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Suggest")
        {
            tut.SendMessage("Part2");
            Destroy(sug);
            Destroy(gameObject);
        }
    }
}
