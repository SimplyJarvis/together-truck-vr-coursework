﻿using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour {

    [SerializeField]
    float speed;
    Rigidbody bulletRb;
	// Use this for initialization
	void Start () {
        bulletRb = GetComponent<Rigidbody>();
        bulletRb.AddForce(transform.forward * speed);
        Destroy(gameObject, 5f);
	}
	
}
